package com.fortytwo.baggagestaff.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fortytwo.baggagestaff.R;
import com.fortytwo.baggagestaff.activity.MainActivity;

/**
 * Created by APN User on 3/16/2018.
 */

public class FlightViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView departure;

    public TextView destination;

    public TextView number;


    public FlightViewHolder(final View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.departure = (TextView) itemView.findViewById(R.id.departure);
        this.destination = (TextView) itemView.findViewById(R.id.destination);
        this.number = (TextView) itemView.findViewById(R.id.number);
        itemView.setOnClickListener(this);
/*        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("CLICK " + departure.getText().toString());
                Intent intent = new Intent(itemView.getContext(), MainActivity.class);
               //intent.putExtra("passengers", passengers);
                intent.putExtra("flight", number.getText());
                itemView.getContext().startActivity(intent);

            }
        });*/
    }

    @Override
    public void onClick(View v) {

    }
}

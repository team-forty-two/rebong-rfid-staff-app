package com.fortytwo.baggagestaff.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.fortytwo.baggagestaff.R;

/**
 * Created by APN User on 3/20/2018.
 */

public class DeviceSelectionActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showDeviceSelector();
    }

    private void showDeviceSelector(){
        String[] items = getResources().getStringArray(R.array.device_selection);
        int itemSelected = 0;
        new AlertDialog.Builder(this)
                .setTitle("Select device name")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                        System.out.println("MainActivity clicked item index is " + selectedIndex);
                    }
                })

                .setCancelable(false)
                .show();
    }



}

package com.fortytwo.baggagestaff.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fortytwo.baggagestaff.R;
import com.fortytwo.baggagestaff.adapter.BaggageAdapter;
import com.fortytwo.baggagestaff.model.Baggage;
import com.fortytwo.baggagestaff.model.Flight;
import com.fortytwo.baggagestaff.model.FlightDetails;
import com.fortytwo.baggagestaff.model.Passenger;
import com.fortytwo.baggagestaff.model.modelnew.Transaction;
import com.fortytwo.baggagestaff.service.BaggageService;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    final Context context = this;
    private EditText message;
    private EditText mobileno;
    private Button sendsms;
    private RecyclerView recyclerView;
    private List<Flight> flights;
    private List<Baggage> baggages = new ArrayList<>();
    private Gson gson;
    private List<Passenger> passengers;
    private SQLiteDatabase sqLiteDatabase;
    private BaggageService baggageService;
    private Retrofit retrofit;
    private final String firebaseUrl = "https://rfid-proj-a2226.firebaseio.com";
    private String deviceNo = null;
    private String baggageNo = null;
    private String flight = null;
    private BaggageAdapter baggageAdapter;
    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.baggageRecyclerView);
        baggageAdapter = new BaggageAdapter(baggages);
        recyclerView.setAdapter(baggageAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        final Intent intent = getIntent();
        //sqLiteDatabase.execSQL("INSERT INTO StaffSettings VALUES('" + intent.getStringExtra("deviceArduino") + "','" + intent.getStringExtra("flight") + "');");
        flights = new ArrayList<>();
        passengers = new ArrayList<>();
        deviceNo = intent.getStringExtra("deviceArduino");
        flight = intent.getStringExtra("flight");

        gson = new GsonBuilder()
                .setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(firebaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        baggageService = retrofit.create(BaggageService.class);

        database = FirebaseDatabase.getInstance();
        final DatabaseReference deviceReference = database.getReference();
        deviceReference.child("transactions/" + deviceNo).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                //for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                System.out.println(" - CHILDREN - " + dataSnapshot.getChildren());
                System.out.println(" - - - BAGGAGE " + dataSnapshot.getKey());
                String baggageNumber = dataSnapshot.getKey();
                fetchBagggageDetails(baggageNumber);
                //}
                   /* Passenger passenger = dataSnapshot.getValue(Passenger.class);
                    passengers.add(passenger);
                    System.out.println("PASSENGER " + passengers.get(0).getFirstName());*/


                //}

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        triggerExit();
    }
*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                DialogInterface.OnClickListener dialog = new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                Intent intent = new Intent(context, FlightSelectionActivity.class);
                                final DatabaseReference transactionReference = database.getReference("transactions/" + deviceNo);
                                final DatabaseReference backupReference = database.getReference("archive");
                                finish();
                                final Call<HashMap<String, Object>> baggages = baggageService.getTransactions(deviceNo);
                                baggages.enqueue(new Callback<HashMap<String, Object>>() {
                                    @Override
                                    public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                                        //response.body();
                                        backupReference.child(deviceNo).child(new Date().toString()).setValue(response.body());
                                        //transactionReference.removeValue();
                                    }

                                    @Override
                                    public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {

                                    }
                                });
                                context.startActivity(intent);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.cancel();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure you want to end the baggage boarding?")
                        .setPositiveButton("Yes", dialog)
                        .setNegativeButton("No", dialog)
                        .show();
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void fetchBagggageDetails(final String baggageNumber) {
        Call<Baggage> call = baggageService.getBaggage(baggageNumber);
        call.enqueue(new Callback<Baggage>() {
            @Override
            public void onResponse(Call<Baggage> call, Response<Baggage> response) {
                Baggage baggage = new Baggage();
                baggage = response.body();
                baggage.setBaggageNumber(baggageNumber);
                System.out.println(" BAGGAGE INSIDE " + baggage.toString());
                System.out.println("PASSENGER FROM BAGGAGE " + baggage.getUser().keySet().toArray(new String[0])[0]);
                String passengerFromBaggage = baggage.getUser().keySet().toArray(new String[0])[0];
                getPassenger(passengerFromBaggage, baggage);
            }

            @Override
            public void onFailure(Call<Baggage> call, Throwable t) {

            }
        });
    }

    private void getPassengerFlight(String passengerID, final Baggage baggage) {
        System.out.println("1");
        Call<HashMap<String, Object>> call = baggageService.getFlightOfPaassenger(passengerID);
        call.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                String smsMesage = null;
                if ((response.body().keySet().toArray(new String[0])[0]).equals(flight)) {
                    System.out.println("4");
                    baggage.setStatus("3");
                    smsMesage = "Your baggage " + baggage.getDescription() + " with baggage number " + baggage.getBaggageNumber() + " was successfully " + " boarded on your flight.";
                } else {
                    System.out.println("5");
                    baggage.setStatus("?");
                    smsMesage = "Your baggage " + baggage.getDescription() + " with baggage number " + baggage.getBaggageNumber() + " was boarded on the wrong flight. Kindly proceed to your flight counter.";
                }
                System.out.println("6");
                baggages.add(baggage);

                baggageAdapter.notifyDataSetChanged();
                System.out.println("BAGGAGE " + baggages.get(0).getStatus());

                //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

                System.out.println("7");
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                System.out.println("8" + t);
            }
        });
    }

    private void getPassenger(String passengerID, final Baggage baggage) {
        System.out.println("1");
        Call<Passenger> call = baggageService.getPassenger(passengerID);
        call.enqueue(new Callback<Passenger>() {
            @Override
            public void onResponse(Call<Passenger> call, Response<Passenger> response) {
                System.out.println("2");
                Passenger passenger = new Passenger();
                passenger = response.body();
                String smsMesage = null;
                if ((passenger.getFlight().keySet().toArray(new String[0])[0]).equals(flight)) {
                    System.out.println("4");
                    baggage.setStatus("3");
                    smsMesage = "Hi " + passenger.getFirstName() + ", \n" + "Your baggage " + baggage.getDescription() + " with baggage number " + baggage.getBaggageNumber() + " was successfully " + " boarded on your flight.";
                } else {
                    System.out.println("5");
                    baggage.setStatus("?");
                    smsMesage = "Hi " + passenger.getFirstName() + ", \n" + "Your baggage " + baggage.getDescription() + " with baggage number " + baggage.getBaggageNumber() + " was boarded on the wrong flight. Kindly proceed to the flight counter immediately.";
                }
                System.out.println("6");
                updateBaggageStatus(baggage);
                baggages.add(baggage);
                baggageAdapter.notifyDataSetChanged();

                //sendMessage(passenger.getContactNumber(), smsMesage);
                System.out.println("BAGGAGE " + baggages.get(0).getStatus());
                System.out.println("PASSENGER CONTACT" + passenger.getContactNumber());
                //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                System.out.println("7");

            }

            @Override
            public void onFailure(Call<Passenger> call, Throwable t) {
                System.out.println("8" + t);
            }
        });
    }

    private void updateBaggageStatus(Baggage baggage) {
        final DatabaseReference baggageReference = database.getReference();
        //baggageReference.child("baggages");
        baggageReference.child("baggages").child(baggage.getBaggageNumber()).child("status").setValue(baggage.getStatus());
    }

    private void sendMessage(String phoneNumber, String message) {

        try {
            System.out.println("- - - - HERE - - - - ");
            SmsManager smsManager = SmsManager.getDefault();

            String strMessage = "Test Message";

            ArrayList<String> listMsgPart = smsManager.divideMessage(message);

            // sendDataMessage () up to 160 char
            smsManager.sendTextMessage(phoneNumber, null, message, null, null);
            // sendMultipartTextMessage() for more than 160 char
            //smsManager.sendMultipartTextMessage(phoneNumber, null, listMsgPart, null, null);

            Toast.makeText(this, "Sent.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            System.out.println(" SMS " + e);
            Toast.makeText(this, "FAILED.", Toast.LENGTH_SHORT).show();
        }
    }

    private void triggerExit() {
        DialogInterface.OnClickListener dialog = new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(context, FlightSelectionActivity.class);
                        finish();
                        context.startActivity(intent);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.cancel();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to end the baggage boarding?")
                .setPositiveButton("Yes", dialog)
                .setNegativeButton("No", dialog)
                .show();
    }

    private boolean checkPermission(String permission) {
        int checkPermission = ContextCompat.checkSelfPermission(this, permission);
        return (checkPermission == PackageManager.PERMISSION_GRANTED);
    }
}

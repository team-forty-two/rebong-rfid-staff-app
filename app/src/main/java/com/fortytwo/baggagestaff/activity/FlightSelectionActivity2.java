package com.fortytwo.baggagestaff.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fortytwo.baggagestaff.model.Devices;
import com.fortytwo.baggagestaff.model.Flight;
import com.fortytwo.baggagestaff.model.modelnew.Baggages;
import com.fortytwo.baggagestaff.model.modelnew.BaseModel;
import com.fortytwo.baggagestaff.model.modelnew.Flights;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by APN User on 3/27/2018.
 */

public class FlightSelectionActivity2 extends AppCompatActivity {

    private final String firebaseUrl = "https://rfid-proj-a2226.firebaseio.com";
    private FirebaseDatabase database;
    private BaseModel baseModel;
    private HashMap<String, ?> map;
    private String baggages;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseModel = new BaseModel();
        map = new HashMap<>();
        //baggages = new String();
        getDeviceAvailable();

        System.out.println(" - 2 - " + baseModel.toString());

    }

    private void getDeviceAvailable() {

        System.out.println(" - 1 - ");
        database = FirebaseDatabase.getInstance();
        final DatabaseReference deviceReference = database.getReference();
        deviceReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //for(DataSnapshot snapshot : dataSnapshot.getChildren()){

                //System.out.println( " BASE MODEL " + dataSnapshot.getValue(BaseModel.class));

                baseModel =  dataSnapshot.getValue(BaseModel.class);
              /* Flights flight = new Flights();
                flight = dataSnapshot.getValue(Flights.class);
                baseModel.setFlights(flight);*/
                /*for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //map = (Map) postSnapshot.getValue();
                    map = (HashMap) postSnapshot.child("baggages").getValue();
                }*/

            }


            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

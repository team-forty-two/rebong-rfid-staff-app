package com.fortytwo.baggagestaff.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fortytwo.baggagestaff.R;
import com.fortytwo.baggagestaff.adapter.FlightAdapter;
import com.fortytwo.baggagestaff.adapter.OnItemClickListener;
import com.fortytwo.baggagestaff.model.Devices;
import com.fortytwo.baggagestaff.model.Flight;

import com.fortytwo.baggagestaff.service.BaggageService;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by APN User on 3/15/2018.
 */

public class FlightSelectionActivity extends AppCompatActivity implements OnItemClickListener {
    final Context context = this;
    private List<Flight> flights;
    private RecyclerView recyclerView;
    private FlightAdapter flightAdapter;
    SQLiteDatabase sqLiteDatabase;
    private String selectedDevice;
    private Devices device;
    private final String firebaseUrl = "https://rfid-proj-a2226.firebaseio.com";
    private FirebaseDatabase database;
    private List<String> devices;
    private BaggageService baggageService;
    private Retrofit retrofit;
    private Gson gson;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight_selection);

        gson = new GsonBuilder()
                .setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(firebaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        baggageService = retrofit.create(BaggageService.class);

        database = FirebaseDatabase.getInstance();

        devices = new ArrayList<String>();
        String[] settings = getSettings();
        System.out.println("LENGTH " + getSettings().length);
        if (settings.length == 0) {
            checkSMSPermission();
            flights = new ArrayList<>();
            //getDeviceNo();
            getDevices();
            //fetchFlights();
        } else {
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("deviceArduino", settings[0]);
            intent.putExtra("flight", settings[1]);
            this.startActivity(intent);
        }
    }

    private void getDeviceNo() {
        database = FirebaseDatabase.getInstance();
        final DatabaseReference deviceReference = database.getReference("devices");
        deviceReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println(" - DEVICE CHILDREN - " + dataSnapshot.getKey());
                getFlightsOnDevice(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
       /* deviceReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    System.out.println(" - DEVICE CHILDREN - " + snapshot.getKey());
                    Devices device = new Devices();
                    device = dataSnapshot.getValue(Devices.class);
                    device.setDeviceNo(snapshot.getKey());
                    devices.add(device);
                    System.out.println(" - DEVICE Values - " + device.toString());
                }
                // }
                //showDeviceSelector();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

    }


    private void getDevices() {
        Call<HashMap<String, Object>> call = baggageService.getDevicesNo();
        call.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                HashMap<String, Object> hashMap = response.body();
                System.out.println(" HASH " + hashMap.keySet());
                for (Map.Entry<String, Object> map : hashMap.entrySet()) {
                    devices.add(map.getKey());
                }
                showDeviceSelector();
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {

            }
        });
    }

    private void getFlightsOnDevice(String deviceNo) {
        database = FirebaseDatabase.getInstance();
        final DatabaseReference deviceReference = database.getReference("devices/" + deviceNo + "/flights");
        deviceReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //System.out.println(" - FLIGHTS Values - " + dataSnapshot.getKey());
                devices.clear();
                fetchFlights(dataSnapshot.getKey());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void showDeviceSelector() {
        final String[] items = new String[devices.size()];
        System.out.println(" - - - SELECT DEVICE - -  -- " + devices.size());
        for (int it = 0; it <= devices.size() - 1; it++) {
            String deviceNo = devices.get(it);
            items[it] = deviceNo;
        }

        new AlertDialog.Builder(this)
                .setTitle("Select device name")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                        System.out.println("MainActivity clicked item index is " + items[selectedIndex]);
                        selectedDevice = items[selectedIndex];
                        setupRecyclerView();
                        getFlightsOnDevice(items[selectedIndex]);
                    }
                })
                .setCancelable(false)
                .show();
    }


    private void fetchFlights(String flightNo) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("flights/" +flightNo);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(" - - -- FLIGHT - - -- " + dataSnapshot.getValue());
                Flight flight = dataSnapshot.getValue(Flight.class);
                System.out.println(" - - -- FLIGHT - - -- " + flight.toString());
                flights.add(flight);
                recyclerView.scrollToPosition(flights.size() - 1);
                flightAdapter.notifyItemInserted(flights.size() - 1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //if (dataSnapshot != null && dataSnapshot.getValue() != null) {

                    //for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        System.out.println(" - - -- FLIGHT - - -- " + dataSnapshot);
                        Flight flight = (Flight) dataSnapshot.getValue();
                        System.out.println(" - - -- FLIGHT - - -- " + flight.toString());
                  //  }
                    *//*
                    flights.add(flight);

                    //System.out.println(" - - -- FLIGHT - - -- " + flight.toString());
                //}

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }

    private void setupRecyclerView() {
        //System.out.println("FLIGHT LIST " + flights);
        //showDeviceSelector();
        recyclerView = (RecyclerView) findViewById(R.id.flightRV);
        flightAdapter = new FlightAdapter(flights);
        flightAdapter.setClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(flightAdapter);
        recyclerView.setHasFixedSize(true);
    }

    private boolean checkPermission(String permission) {
        int checkPermission = ContextCompat.checkSelfPermission(this, permission);
        return (checkPermission == PackageManager.PERMISSION_GRANTED);
    }

    private void checkSMSPermission() {
        if (checkPermission(Manifest.permission.SEND_SMS)) {
            System.out.println("ALLOWED SMS");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS},
                    111);
        }
    }

    private String[] getSettings() {
        sqLiteDatabase = openOrCreateDatabase("staffDb", MODE_PRIVATE, null);
        String arduinoId = null;
        String flightNo = null;
        String[] settings = {};
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Passenger;");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS StaffSettings(arduinoID VARCHAR, flightNo VARCHAR);");
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM StaffSettings", null);
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            cursor.moveToLast();
            settings[0] = cursor.getString(0);
            settings[1] = cursor.getString(1);
        }
        return settings;
    }

    @Override
    public void onClick(View view, int position) {
        Flight flight = flights.get(position);
        System.out.println("CLICK " + flight.getNumber() + " DEVICE " + selectedDevice);

        Intent intent = new Intent(view.getContext(), MainActivity.class);
        intent.putExtra("deviceArduino", selectedDevice);
        intent.putExtra("flight", flight.getNumber());
        this.startActivity(intent);
        finish();
    }
}

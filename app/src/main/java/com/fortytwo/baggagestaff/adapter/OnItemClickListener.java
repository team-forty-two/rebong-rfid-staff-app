package com.fortytwo.baggagestaff.adapter;

import android.view.View;

/**
 * Created by APN User on 3/20/2018.
 */

public interface OnItemClickListener {
    public void onClick(View view, int position);
}

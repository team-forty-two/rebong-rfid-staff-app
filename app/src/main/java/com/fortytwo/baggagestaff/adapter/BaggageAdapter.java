package com.fortytwo.baggagestaff.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fortytwo.baggagestaff.R;
import com.fortytwo.baggagestaff.model.Baggage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by APN User on 3/22/2018.
 */

public class BaggageAdapter extends RecyclerView.Adapter<BaggageAdapter.BaggageViewHolder> {

    private List<Baggage> listBaggage;

    public BaggageAdapter(List<Baggage> baggages){
        listBaggage=baggages;
    }

    public void clear() {
        listBaggage.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Baggage> list) {
        listBaggage.addAll(list);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public BaggageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.baggage_list,parent,false);
        return new BaggageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaggageViewHolder holder, int position) {
        Baggage baggage = listBaggage.get(position);
        String status = baggage.getStatus();
        holder.status.setText(status);
        switch ((status)){
            case "1":holder.status.setBackgroundResource(R.color.colorFirstStation); break;
            case "2":holder.status.setBackgroundResource(R.color.colorSecondStation); break;
            case "3":holder.status.setBackgroundResource(R.color.colorThirdStation); break;
            default: holder.status.setBackgroundResource(R.color.colorUnknownStation); break;
        }

        holder.baggageNumber.setText(baggage.getBaggageNumber());
        holder.baggageDescription.setText(baggage.getDescription());
    }

    @Override
    public int getItemCount() {
        return listBaggage.size();
    }

    public class BaggageViewHolder extends RecyclerView.ViewHolder {

        public TextView baggageNumber;

        public TextView status;

        public TextView baggageDescription;



        public BaggageViewHolder(View itemView) {
            super(itemView);
            baggageNumber = (TextView) itemView.findViewById(R.id.baggageNumber);
            status = (TextView) itemView.findViewById(R.id.statusText);
            baggageDescription = (TextView) itemView.findViewById(R.id.baggageDescription);
        }


    }
}

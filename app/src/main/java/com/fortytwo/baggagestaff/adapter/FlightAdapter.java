package com.fortytwo.baggagestaff.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fortytwo.baggagestaff.R;
import com.fortytwo.baggagestaff.holder.FlightViewHolder;
import com.fortytwo.baggagestaff.model.Flight;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by APN User on 3/16/2018.
 */

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.FlightViewHolder> {
    private  List<Flight> flights;

    FlightViewHolder flightViewHolder;

    private OnItemClickListener clickListener;

    public FlightAdapter(List<Flight> flightList) {
        this.flights = flightList;
    }

    public void clear() {
        flights.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Flight> flightList) {
        flights.addAll(flightList);
        notifyDataSetChanged();
    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return flights.size();
    }

    @NonNull
    @Override
    public FlightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_list, parent, false);
        return new FlightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlightViewHolder holder, int position) {
        Flight flight = flights.get(position);
        // TODO replace findViewById by ViewHolder
        holder.departure.setText(flight.getDeparture());
        holder.destination.setText(flight.getDestination());
        holder.number.setText(flight.getNumber());
        //holder.passengers = new Gson().toJson(flight.getPassengers());

    }

    public class FlightViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView departure;

        public TextView destination;

        public TextView number;


        public FlightViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.departure = (TextView) itemView.findViewById(R.id.departure);
            this.destination = (TextView) itemView.findViewById(R.id.destination);
            this.number = (TextView) itemView.findViewById(R.id.number);

/*        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("CLICK " + departure.getText().toString());
                Intent intent = new Intent(itemView.getContext(), MainActivity.class);
               //intent.putExtra("passengers", passengers);
                intent.putExtra("flight", number.getText());
                itemView.getContext().startActivity(intent);

            }
        });*/
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());

        }
    }


}
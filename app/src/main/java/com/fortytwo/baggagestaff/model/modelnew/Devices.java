package com.fortytwo.baggagestaff.model.modelnew;

import com.fortytwo.baggagestaff.model.Flight;

import java.util.HashMap;

/**
 * Created by APN User on 3/25/2018.
 */

public class Devices {

    HashMap<String,Baggages> baggages;

    HashMap<String, Flights> flights;

    String deviceNo;

    public HashMap<String, Baggages> getBaggages() {
        return baggages;
    }

    public void setBaggages(HashMap<String, Baggages> baggages) {
        this.baggages = baggages;
    }

    public HashMap<String, Flights> getFlights() {
        return flights;
    }

    public void setFlights(HashMap<String, Flights> flights) {
        this.flights = flights;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }
}

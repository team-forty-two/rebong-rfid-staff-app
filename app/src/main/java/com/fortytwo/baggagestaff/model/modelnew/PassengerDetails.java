package com.fortytwo.baggagestaff.model.modelnew;

import com.fortytwo.baggagestaff.model.Flight;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */

public class PassengerDetails {

    HashMap<String, Baggages> baggages;

    String contactNumber;

    String firstName;

    HashMap<String, Object> flight;

    String lastName;

    public HashMap<String, Baggages> getBaggages() {
        return baggages;
    }

    public void setBaggages(HashMap<String, Baggages> baggages) {
        this.baggages = baggages;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public HashMap<String, Object> getFlight() {
        return flight;
    }

    public void setFlight(HashMap<String, Object> flight) {
        this.flight = flight;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

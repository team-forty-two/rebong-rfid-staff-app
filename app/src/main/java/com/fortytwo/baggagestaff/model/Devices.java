package com.fortytwo.baggagestaff.model;

import java.util.HashMap;

/**
 * Created by APN User on 3/25/2018.
 */

public class Devices {

    HashMap<String, Object> baggages;

    HashMap<String, Object> flights;

    String deviceNo;


    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public HashMap<String, Object> getBaggages() {
        return baggages;
    }

    public void setBaggages(HashMap<String, Object> baggages) {
        this.baggages = baggages;
    }

    public HashMap<String, Object> getFlights() {
        return flights;
    }

    public void setFlights(HashMap<String, Object> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        return "Devices{" +
                "baggages=" + baggages +
                ", flights=" + flights +
                ", deviceNo='" + deviceNo + '\'' +
                '}';
    }
}

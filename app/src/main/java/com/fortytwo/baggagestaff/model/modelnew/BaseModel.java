package com.fortytwo.baggagestaff.model.modelnew;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */

@IgnoreExtraProperties
public class BaseModel {

    Baggages baggages;

    Devices devices;

    Flights flights;

    Passengers passengers;

    public BaseModel() {
    }

    public BaseModel(Baggages baggages, Devices devices, Flights flights, Passengers passengers) {
        this.baggages = baggages;
        this.devices = devices;
        this.flights = flights;
        this.passengers = passengers;
    }

    public Baggages getBaggages() {
        return baggages;
    }

    public void setBaggages(Baggages baggages) {
        this.baggages = baggages;
    }

    public Devices getDevices() {
        return devices;
    }

    public void setDevices(Devices devices) {
        this.devices = devices;
    }

    public Flights getFlights() {
        return flights;
    }

    public void setFlights(Flights flights) {
        this.flights = flights;
    }

    public Passengers getPassengers() {
        return passengers;
    }

    public void setPassengers(Passengers passengers) {
        this.passengers = passengers;
    }


    @Override
    public String toString() {
        return "BaseModel{" +
                "baggages=" + baggages +
                ", devices=" + devices +
                ", flights=" + flights +
                ", passengers=" + passengers +
                '}';
    }
}

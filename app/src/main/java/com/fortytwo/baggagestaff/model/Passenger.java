package com.fortytwo.baggagestaff.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APN User on 3/20/2018.
 */

public class Passenger {
    String contactNumber;

    String firstName;

    String lastName;

    HashMap<String, Object> baggages;

    HashMap<String, Object> flight;

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public HashMap<String, Object> getBaggages() {
        return baggages;
    }

    public void setBaggages(HashMap<String, Object> baggages) {
        this.baggages = baggages;
    }

    public HashMap<String, Object> getFlight() {
        return flight;
    }

    public void setFlight(HashMap<String, Object> flight) {
        this.flight = flight;
    }
}

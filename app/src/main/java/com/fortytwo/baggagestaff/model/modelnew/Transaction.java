package com.fortytwo.baggagestaff.model.modelnew;

import java.util.HashMap;

/**
 * Created by APN User on 4/1/2018.
 */

public class Transaction {

    private HashMap<String, Object> baggageNumber;

    public HashMap<String, Object> getBaggageNumber() {
        return baggageNumber;
    }

    public void setBaggageNumber(HashMap<String, Object> baggageNumber) {
        this.baggageNumber = baggageNumber;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "baggageNumber=" + baggageNumber +
                '}';
    }
}

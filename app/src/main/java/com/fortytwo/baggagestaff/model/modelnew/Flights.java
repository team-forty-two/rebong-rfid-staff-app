package com.fortytwo.baggagestaff.model.modelnew;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */

public class Flights {

    HashMap<String, FlightDetails> flightDetails;

    public HashMap<String, FlightDetails> getFlightDetails() {
        return flightDetails;
    }

    public void setFlightDetails(HashMap<String, FlightDetails> flightDetails) {
        this.flightDetails = flightDetails;
    }
}

package com.fortytwo.baggagestaff.model.modelnew;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */
@IgnoreExtraProperties
public class Baggages {


    private HashMap<String, BaggageDetails> baggageDetails;

    public HashMap<String, BaggageDetails> getBaggageDetails() {
        return baggageDetails;
    }

    public Baggages() {
    }

    public Baggages(HashMap<String, BaggageDetails> baggageDetails) {
        this.baggageDetails = baggageDetails;
    }

    public void setBaggageDetails(HashMap<String, BaggageDetails> baggageDetails) {
        this.baggageDetails = baggageDetails;
    }

    @Override
    public String toString() {
        return "Baggages{" +
                "baggageDetails=" + baggageDetails +
                '}';
    }
}

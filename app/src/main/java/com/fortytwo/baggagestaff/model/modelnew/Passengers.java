package com.fortytwo.baggagestaff.model.modelnew;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */

public class Passengers {

    HashMap<String, PassengerDetails> passengerDetails;

    public HashMap<String, PassengerDetails> getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(HashMap<String, PassengerDetails> passengerDetails) {
        this.passengerDetails = passengerDetails;
    }
}

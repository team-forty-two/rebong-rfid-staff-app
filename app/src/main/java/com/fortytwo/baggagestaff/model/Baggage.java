package com.fortytwo.baggagestaff.model;

import java.util.HashMap;

/**
 * Created by APN User on 3/22/2018.
 */

public class Baggage {

    String baggageNumber;

    String status;

    String description;

    HashMap<String, Object> user;

    HashMap<String, Object> devices;

    public Baggage() {
    }

    public Baggage(String baggageNumber, String status, String description, HashMap<String, Object> user, HashMap<String, Object> devices) {
        this.baggageNumber = baggageNumber;
        this.status = status;
        this.description = description;
        this.user = user;
        this.devices = devices;
    }

    public String getBaggageNumber() {
        return baggageNumber;
    }

    public void setBaggageNumber(String baggageNumber) {
        this.baggageNumber = baggageNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashMap<String, Object> getUser() {
        return user;
    }

    public void setUser(HashMap<String, Object> user) {
        this.user = user;
    }

    public HashMap<String, Object> getDevices() {
        return devices;
    }

    public void setDevices(HashMap<String, Object> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        return "Baggage{" +
                "baggageNumber='" + baggageNumber + '\'' +
                ", status='" + status + '\'' +
                ", description='" + description + '\'' +
                ", user=" + user +
                ", devices=" + devices +
                '}';
    }
}
package com.fortytwo.baggagestaff.model.modelnew;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */

public class BaggageDetails {

    String status;

    String description;

    HashMap<String, Object> user;

    HashMap<String, Object> devices;

    public BaggageDetails() {
    }

    public BaggageDetails(String status, String description, HashMap<String, Object> user, HashMap<String, Object> devices) {
        this.status = status;
        this.description = description;
        this.user = user;
        this.devices = devices;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashMap<String, Object> getUser() {
        return user;
    }

    public void setUser(HashMap<String, Object> user) {
        this.user = user;
    }

    public HashMap<String, Object> getDevices() {
        return devices;
    }

    public void setDevices(HashMap<String, Object> devices) {
        this.devices = devices;
    }
}

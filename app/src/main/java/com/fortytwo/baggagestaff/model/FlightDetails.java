package com.fortytwo.baggagestaff.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by APN User on 3/7/2018.
 */

public class FlightDetails {
    String departure;
    String destination;
    String number;

    HashMap<String, Object> passengers;




    public FlightDetails(){

    }

    public FlightDetails(String departure, String destination, String number, HashMap<String, Object> passengers) {
        this.departure = departure;
        this.destination = destination;
        this.number = number;
        this.passengers = passengers;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public HashMap<String, Object> getPassengers() {
        return passengers;
    }

    public void setPassengers(HashMap<String, Object> passengers) {
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        return "FlightDetails{" +
                "departure='" + departure + '\'' +
                ", destination='" + destination + '\'' +
                ", number='" + number + '\'' +
                ", passengers=" + passengers +
                '}';
    }
}

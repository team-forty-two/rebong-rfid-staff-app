package com.fortytwo.baggagestaff.model.modelnew;

import java.util.HashMap;

/**
 * Created by APN User on 3/27/2018.
 */

public class FlightDetails {

    String departure;
    String destination;
    String number;

    HashMap<String, Object> passengers;

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public HashMap<String, Object> getPassengers() {
        return passengers;
    }

    public void setPassengers(HashMap<String, Object> passengers) {
        this.passengers = passengers;
    }
}

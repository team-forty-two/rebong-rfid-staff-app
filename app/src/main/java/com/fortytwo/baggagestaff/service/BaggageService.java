package com.fortytwo.baggagestaff.service;

import com.fortytwo.baggagestaff.model.Baggage;
import com.fortytwo.baggagestaff.model.Flight;
import com.fortytwo.baggagestaff.model.Passenger;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by APN User on 3/22/2018.
 */

public interface BaggageService {

    @GET("/baggages/{baggageId}.json")
    Call<Baggage> getBaggage(@Path("baggageId") String baggage);

    @GET("/passengers/{passengerId}.json")
    Call<Passenger> getPassenger(@Path("passengerId") String passengerId);

    @GET("/passengers/{passengerId}/flight.json")
    Call<HashMap<String,Object>> getFlightOfPaassenger(@Path("passengerId") String passengerId);

    @GET("/flights/{flightNo}")
    Call<Flight> getFlight(String flightNo);

    @GET("/devices.json")
    Call<HashMap<String, Object>> getDevicesNo();

    @GET("/transactions/{deviceNo}.json")
    Call<HashMap<String, Object>> getTransactions(@Path("deviceNo")String deviceNo);




}
